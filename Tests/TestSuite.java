import Numeric.ComplexNumber;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Range of tests for calculator program
 * @author petersjt
 * @author savvap
 */

public class TestSuite {

    Calculator calc;
    ComplexNumber posPos, posNeg, negPos, negNeg;

    @Before
    public void setUp(){
        calc = new Calculator();
        posPos = new ComplexNumber(3, 2);
        posNeg = new ComplexNumber(3, -2);
        negPos = new ComplexNumber(-3, 2);
        negNeg = new ComplexNumber(-3, -2);
    }


    /* User Story 01 Tests*/

    @Test
    public void testCalculatorAcceptsComplexNumberWithoutComplexPart() {
        ComplexNumber number = new ComplexNumber(3, 0);

        ComplexNumber parsedNumber = calc.parse("3");

        assert number.equals(parsedNumber);
    }

    @Test
    public void testCalculatorAcceptsComplexNumberWithoutRealPart() {
        ComplexNumber number = new ComplexNumber(0, 5);

        ComplexNumber parsedNumber = calc.parse("5i");

        assert number.equals(parsedNumber);
    }

    @Test
    public void testCalculatorAcceptsComplexNumberWithRealAndComplexParts() {
        ComplexNumber number = new ComplexNumber(3, 5);

        ComplexNumber parsedNumber = calc.parse("3+5i");

        assert parsedNumber.equals(number);
    }

    @Test
    public void testCalculatorAcceptsNegativeComplexNumberWithRealAndComplexParts() {
        ComplexNumber number = new ComplexNumber(-3, -5);

        ComplexNumber parsedNumber = calc.parse("-3-5i");

        assert parsedNumber.equals(number);
    }

    /* User Story 01 Tests End*/

    /* User Story 02 Tests*/

    @Test
    public void testCalculatorAddsComplexNumbersWithoutComplexParts() {
        ComplexNumber numberA = new ComplexNumber(3, 0);
        ComplexNumber numberB = new ComplexNumber(5, 0);
        ComplexNumber result = new ComplexNumber(8, 0);

        ComplexNumber calculatedResult = calc.add(numberA, numberB);


        assert result.equals(calculatedResult);
    }

    @Test
    public void testCalculatorAddsComplexNumbersWithoutRealParts() {
        ComplexNumber numberA = new ComplexNumber(0, 3);
        ComplexNumber numberB = new ComplexNumber(0, 5);
        ComplexNumber result = new ComplexNumber(0, 8);

        ComplexNumber calculatedResult = calc.add(numberA, numberB);


        assert result.equals(calculatedResult);
    }

    @Test
    public void testCalculatorAddsComplexNumbersWithComplexAndRealParts() {
        ComplexNumber numberA = new ComplexNumber(3, 2);
        ComplexNumber numberB = new ComplexNumber(5, 4);
        ComplexNumber result = new ComplexNumber(8, 6);

        ComplexNumber calculatedResult = calc.add(numberA, numberB);


        assert result.equals(calculatedResult);
    }

    /* User Story 02 Tests End*/

    /* User Story 03 Tests*/

    @Test
    public void testCalculatorSubtractsComplexNumbersWithoutComplexParts() {
        ComplexNumber numberA = new ComplexNumber(3, 0);
        ComplexNumber numberB = new ComplexNumber(5, 0);
        ComplexNumber result = new ComplexNumber(-2, 0);

        ComplexNumber calculatedResult = calc.subtract(numberA, numberB);


        assert result.equals(calculatedResult);
    }

    @Test
    public void testCalculatorSubtractsComplexNumbersWithoutRealParts() {
        ComplexNumber numberA = new ComplexNumber(0, 3);
        ComplexNumber numberB = new ComplexNumber(0, 5);
        ComplexNumber result = new ComplexNumber(0, -2);

        ComplexNumber calculatedResult = calc.subtract(numberA, numberB);


        assert result.equals(calculatedResult);
    }

    @Test
    public void testCalculatorSubtractsComplexNumbersWithComplexAndRealParts() {
        ComplexNumber numberA = new ComplexNumber(3, 2);
        ComplexNumber numberB = new ComplexNumber(5, 4);
        ComplexNumber result = new ComplexNumber(-2, -2);

        ComplexNumber calculatedResult = calc.subtract(numberA, numberB);


        assert result.equals(calculatedResult);
    }

    /* User Story 03 Tests End*/

    @Test
    public void testMultiply(){ //story 04

        //numbers are 3, 2i; they are positive || negative

        ComplexNumber posPosXPosPos =  calc.multiply(posPos, posPos);
        System.out.println(posPosXPosPos.toString());
        assert posPosXPosPos.getReal() == 5;
        assert posPosXPosPos.getImaginary() == 12;

        ComplexNumber posPosXPosNeg = calc.multiply(posPos, posNeg);
        System.out.println(posPosXPosNeg.toString());
        assert posPosXPosNeg.getReal() == 13;
        assert posPosXPosNeg.getImaginary() == 0;

        ComplexNumber posNegXNegPos = calc.multiply(posNeg, negPos);
        System.out.println(posNegXNegPos.toString());
        assert posNegXNegPos.getReal() == -5;
        assert posNegXNegPos.getImaginary() == 12;

        ComplexNumber negPosXNegNeg = calc.multiply(negPos, negNeg);
        System.out.println(negPosXNegNeg.toString());
        assert negPosXNegNeg.getReal() == 13;
        assert negPosXNegNeg.getImaginary() == 0;

        ComplexNumber negNegXPosPos = calc.multiply(negNeg, posPos);
        System.out.println(negNegXPosPos.toString());
        assert negNegXPosPos.getReal() == -5;
        assert negNegXPosPos.getImaginary() == -12;

        ComplexNumber negNegXNegNeg = calc.multiply(negNeg, negNeg);
        System.out.println(negNegXNegNeg);
        assert negNegXNegNeg.getReal() == 5;
        assert negNegXNegNeg.getImaginary() == 12;
    }

    @Test
    public void testDisplayHistory(){ //Story 05

        String intendedResult = "Operation History\n" +
                "------------------------------------\n" +
                "1)\t(3 + 2i) + (3 + 2i) = 6 + 4i\n" +
                "2)\t(-3 -2i) * (3 + 2i) = -5 -12i";

        ComplexNumber a = calc.add(posPos, posPos);
        ComplexNumber b = calc.multiply(negNeg,negPos);

        ByteArrayOutputStream content = new ByteArrayOutputStream();
        System.setOut(new PrintStream(content));
        Calculator.displayHistory(calc);

        System.setOut(new PrintStream(System.out));

        assert content.toString().contains(intendedResult);
    }

    @Test
    public void testSubstitute(){ //Story 06
        ComplexNumber a = calc.add(negNeg, negNeg);
        System.out.println(a.toString());
        ComplexNumber b = calc.multiply(negPos, negNeg);
        System.out.println(b.toString());


    }

    @Test
    public void testClearHistory(){ //Story 07
        ComplexNumber c = calc.add(posPos, posPos);
        calc.clearHistory();
        assert calc.getHistory().size() == 0;
    }

    @Test
    public void testClearWindow(){ //Story 08
        ComplexNumber c = calc.add(posPos, posPos);
        calc.clearWindow();
        //test does not appear possible with junit due to the nature of the console
    }

}
