import Numeric.ComplexNumber;
import Numeric.Operation;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Blake on 1/9/2017.
 */
public class Calculator {
    private List<Operation> history;

    public static void main(String[] args) {
        Calculator c = new Calculator();
        c.printHelp();
        String command = "";
        Scanner s = new Scanner(System.in);
        while (!command.equalsIgnoreCase("quit")) {
            System.out.print("Command: ");
            command = s.nextLine().toLowerCase();
            if (command.equals("help")) {
                c.printHelp();
            } else if (command.equals("add")) {
                ComplexNumber[] nums = c.getValidInput(s, new ComplexNumber[2], c);

                if (nums != null) {
                    System.out.println("Result: " + c.add(nums[0], nums[1]));
                } else {
                    System.out.println("Operation canceled");
                }
            } else if (command.equals("multiply")) {
                ComplexNumber[] nums = c.getValidInput(s, new ComplexNumber[2], c);
                if (nums != null) {
                    System.out.println("Result: " + c.multiply(nums[0], nums[1]));
                } else {
                    System.out.println("Operation canceled");
                }
            } else if (command.equals("subtract")) {
                ComplexNumber[] nums = c.getValidInput(s, new ComplexNumber[2], c);
                if (nums != null) {
                    System.out.println("Result: " + c.subtract(nums[0], nums[1]));
                } else {
                    System.out.println("Operation canceled");
                }
            } else if (command.equals("history")) {
                displayHistory(c);
            } else if (command.equals("clear")) {
                c.clearWindow();
            } else if (command.equals("clear history")) {
                c.clearHistory();
            } else if (command.equals("quit")) {
            } else {
                System.out.println("Invalid command entered. Please review to help for a list of commands");
            }
        }
    }



    /**
     * Fills the specified array with valid inputs
     *
     * @param s   the scanner to read from
     * @param arr the array to fill
     * @param c
     * @return null if canceled, otherwise arr.length valid complex numbers
     */
    private ComplexNumber[] getValidInput(Scanner s, ComplexNumber[] arr, Calculator c) {
        int n = 0;
        while (n < arr.length) {
            System.out.print("Enter a complex number: ");
            String next = s.nextLine().toLowerCase().trim();
            if (next.equals("cancel")) {
                return null;
            }
            arr[n] = c.parse(next);
            if (arr[n] != null) {
                n++;
            } else {
                System.out.println("Invalid complex number or backreference entered. Enter a valid complex number or cancel");
            }
        }
        return arr;
    }

    public Calculator() {
        history = new LinkedList<>();
    }

    private void printHelp() {
        System.out.println("=============================");
        System.out.println("  Complex Number Calculator  ");
        System.out.println("-----------------------------");
        System.out.println("<Command> ..... <Description>");
        System.out.println("<Commands Case Insensitive>");
        System.out.println("help .............. show help");
        System.out.println("add ..................... add");
        System.out.println("subtract ........... subtract");
        System.out.println("multiply ........... multiply");
        System.out.println("history ........ show history");
        System.out.println("clear ...... clear cmd prompt");
        System.out.println("clear history . clear history");
        System.out.println("quit ........... exit the app");
        System.out.println("cancel .... cancel current op");

        System.out.println("-----------------------------");

        System.out.println("When prompted, enter a complex number in the form: 5+2i , 5 , 2i , 5-2i, or !3 , where the latter is a backreference to a previous result");
    }

    public static void displayHistory(Calculator c) {
        System.out.println("Operation History");
        System.out.println("------------------------------------");
        List<Operation> h = c.getHistory();
        int i = 0;
        for (Operation o : h) {
            System.out.println(++i + ")\t" + o);
        }
    }

    /**
     * multiplies two complex numbers
     *
     * @param a complex number
     * @param b complex number
     * @return a * b
     */
    public ComplexNumber multiply(ComplexNumber a, ComplexNumber b) {
        ComplexNumber result = new ComplexNumber(a.getReal() * b.getReal() - a.getImaginary() * b.getImaginary(),
                a.getReal() * b.getImaginary() + a.getImaginary() * b.getReal());
        history.add(new Operation(a, b, result, "*"));
        return result;
    }

    /**
     * adds two complex numbers
     *
     * @param a complex number
     * @param b complex number
     * @return a + b
     */
    public ComplexNumber add(ComplexNumber a, ComplexNumber b) {
        ComplexNumber result = new ComplexNumber(a.getReal() + b.getReal(), a.getImaginary() + b.getImaginary());
        history.add(new Operation(a, b, result, "+"));
        return result;
    }

    /**
     * subtracts two complex numbers
     *
     * @param a complex number
     * @param b complex number
     * @return a - b
     */
    public ComplexNumber subtract(ComplexNumber a, ComplexNumber b) {
        ComplexNumber result = new ComplexNumber(a.getReal() - b.getReal(), a.getImaginary() - b.getImaginary());
        history.add(new Operation(a, b, result, "-"));
        return result;
    }



    /**
     * Parses an input string and returns its complex number representation
     *
     * @param input string to parse
     * @return the complex number representation, null if no match
     */
    public ComplexNumber parse(String input) {

        Pattern pattern = Pattern.compile("^!(\\d*)$");
        Matcher matcher = pattern.matcher(input);
        if (matcher.matches()) {
            try {
                return parseHistoricalResult(matcher.group(1).replaceAll("\\s", ""));
            } catch (IllegalArgumentException e) {
                return null;
            }
        }

        pattern = Pattern.compile("^((\\s*-\\s*)?\\d+(?=[^i]|$))?(\\s*\\+\\s*)?(((\\s*-\\s*)?\\d*)i)?$");
        matcher = pattern.matcher(input);
        if (!matcher.matches()) {
            return null;
        }
        try {
            String r = matcher.group(1) == null ? "0" : matcher.group(1),
                    i = matcher.group(5) == null ? "0" :
                            (matcher.group(5).length() == 0 || matcher.group(5).equals("-") ?
                                    matcher.group(5) + "1" : matcher.group(5));
            return new ComplexNumber(Integer.parseInt(r.replaceAll("\\s", "")), Integer.parseInt(i.replaceAll("\\s", "")));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public List<Operation> getHistory() {
        return history;
    }

    /**
     * Parses a backreference number string and obtains the historical result
     *
     * @param input backreference number string, e.g. "3" to reference the third historical result
     * @return the specified result, or null if invalid
     */
    public ComplexNumber parseHistoricalResult(String input) {
        int n = Integer.parseInt(input);
        if (n < 0 || n > history.size() - 1) {
            return null;
        } else {
            return history.get(n).result;
        }
    }

    /**
     * Clears the history of the console
     */
    public void clearHistory() {
        history.clear();
    }

    /**
     * Clears the calculator console
     */
    public void clearWindow() {
        try {
            Runtime.getRuntime().exec("cls");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
