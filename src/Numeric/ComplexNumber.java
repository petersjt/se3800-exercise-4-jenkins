package Numeric;

/**
 * Represents a complex number in terms of two integers for the real/ imaginary components
 */
public class ComplexNumber {
    public void setReal(int real) {
        this.real = real;
    }

    public void setImaginary(int imaginary) {
        this.imaginary = imaginary;
    }

    private int real, imaginary;

    public ComplexNumber(int real, int imaginary) {
        this.real = real;
        this.imaginary = imaginary;
    }

    public int getReal() {
        return real;
    }

    public int getImaginary() {
        return imaginary;
    }


    public String toString() {
        String result = getReal() + "";
        if(getImaginary()!=0){
            if(getImaginary()<0){
                result += " " + getImaginary();
            } else {
                result += " + " + getImaginary();
            }
            result += "i";
        }
        return result;
    }

    @Override
    public boolean equals(Object a){
        if (a == null || !(a instanceof ComplexNumber)) {
            return false;
        }

        if (a == this) {
            return true;
        }

        return this.getReal() == ((ComplexNumber)a).getReal() && this.getImaginary() == ((ComplexNumber)a).getImaginary();
    }
}