package Numeric;

/**
 * Created by Blake on 1/9/2017.
 */
public class Operation {
    private String operator;
    public ComplexNumber a, b, result;

    public String toString() {
        return "(" + a.toString() + ") " + operator + " (" + b.toString() + ") = " + result.toString();
    }

    /**
     * Initializes the operation with two complex numbers
     *
     * @param a operand a
     * @param b operand b
     */
    public Operation(ComplexNumber a, ComplexNumber b, ComplexNumber result, String operator) {
        this.a = a;
        this.b = b;
        this.result = result;
        this.operator=operator;
    }
}
